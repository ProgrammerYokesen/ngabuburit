<!DOCTYPE html>
<html lang="en">

<head>
    @laravelPWA
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ngabuburit</title>


    <!-- CSS Global -->
    <link href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/owlcarousel2/assets/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/owlcarousel2/assets/owl.theme.default.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/prettyphoto/css/prettyPhoto.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/animate/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/countdown/jquery.countdown.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/css/theme.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-193993142-1"></script>

    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
    <script>
      window.OneSignal = window.OneSignal || [];
      OneSignal.push(function() {
        OneSignal.init({
          appId: "7b8aa180-38b9-4ad9-a3fa-29356d60a5bf",
        });
      });
    </script>

    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-193993142-1');
    </script>
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '835286217080019');
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=835286217080019&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->

</head>

<body id="home" class="wide body-light">
    <!-- Preloader -->
    {{-- <div id="preloader">
    <div id="status">
        <div class="spinner"></div>
    </div>
</div> --}}

    <!-- Wrap all content -->
    <div class="wrapper">

        <!-- HEADER -->
        <header class="header fixed">
            <div class="container">
                <div class="header-wrapper clearfix">

                    <!-- Logo -->
                    {{-- <div class="logo">
                <a href="#home" class="scroll-to">
                    <span class="fa-stack">
                        <i class="fa logo-hex fa-stack-2x"></i>
                        <i class="fa logo-fa fa-map-marker fa-stack-1x"></i>
                    </span>
                    Ngabuburit
                </a>
            </div> --}}
                    <!-- /Logo -->

                    <!-- Navigation -->
                    <div id="mobile-menu"></div>
                    {{-- <nav class="navigation closed clearfix">
                <a href="#" class="menu-toggle btn"><i class="fa fa-bars"></i></a>
                <ul class="sf-menu nav">
                    <li class="active"><a href="#home">Home</a></li>
                    <li><a href="#about">About</a></li>
                    <li><a href="#schedule">Schedule</a></li>
                    <li><a href="#sponsors">Sponsors</a></li>
                    <li><a href="#speakers">Speakers</a></li>
                    <li><a href="#price">Price</a></li>
                    <li><a href="#location">Location</a></li>
                    <li><a href="blog.html">Blog</a></li>
                </ul>
            </nav> --}}
                    <!-- /Navigation -->

                </div>
            </div>
        </header>
        <!-- /HEADER -->

        <!-- Content area -->
        <div class="content-area">

            <div id="main">
                <!-- SLIDER -->
                <section class="page-section no-padding background-img-slider">
                    <div class="container">

                        <div id="main-slider" class="owl-carousel owl-theme">

                            <!-- Slide -->
                            <div class="item page text-center slide0">
                                <div class="caption">
                                    <div class="container">
                                        <div class="div-table">
                                            <div class="div-cell">
                                                <h2 class="caption-title" data-animation="fadeInDown"
                                                    data-animation-delay="100"><span>January 17-19, 2014</span></h2>
                                                <h3 class="caption-subtitle" data-animation="fadeInUp"
                                                    data-animation-delay="300">PHP Conference ın Istanbul</h3>
                                                <div class="countdown-wrapper">
                                                    <div id="defaultCountdown" class="defaultCountdown clearfix"></div>
                                                </div>
                                                <p class="caption-text">
                                                    <a class="btn btn-theme btn-theme-xl scroll-to" href="#register"
                                                        data-animation="flipInY" data-animation-delay="600"> Register <i
                                                            class="fa fa-arrow-circle-right"></i></a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>
                <!-- /SLIDER -->
            </div>


            <!-- PAGE SCHEDULE -->
            <section class="page-section light" id="schedule">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 pull-left">
                            <h1 class="section-title">
                                <span data-animation="flipInY" data-animation-delay="300" class="icon-inner"><span
                                        class="fa-stack"><i class="fa rhex fa-stack-2x"></i><i
                                            class="fa fa-star fa-stack-1x"></i></span></span>
                                <span data-animation="fadeInRight" data-animation-delay="500"
                                    class="title-inner">Jadwal</span>
                            </h1>
                        </div>
                        <div class="col-md-4 text-right-md pull-right">
                            <a href="#" class="btn btn-theme btn-theme-lg btn-theme-transparent-grey pull-right"
                                data-animation="flipInY" data-animation-delay="300"><i class="fa fa-print"></i> Download
                                Schedule</a>
                        </div>
                    </div>

                    <!-- Schedule -->
                    <div class="schedule-wrapper clear" data-animation="fadeIn" data-animation-delay="200">
                        <div class="schedule-tabs lv1">
                            <ul id="tabs-lv1" class="nav nav-justified">
                                <li class="active"><a href="#tab-first" data-toggle="tab"><strong>ACARA HARI
                                            INI</strong></a></li>
                                {{-- <li><a href="#tab-second" data-toggle="tab"><strong>STUDIO LOUNGE</strong> <br/></a></li>
                            <li><a href="#tab-third" data-toggle="tab"><strong>STUDIO HIBURAN</strong> <br/></a></li>
                            <li><a href="#tab-last" data-toggle="tab"><strong>WEBINAR</strong></a></li> --}}
                            </ul>
                        </div>
                        <div class="tab-content lv1">
                            <!-- tab1 -->
                            <div id="tab-first" class="tab-pane fade in active">
                                <div class="schedule-tabs lv2">
                                    <ul id="tabs-lv21" class="nav nav-justified">
                                        <li class="active"><a href="#tab-lv21-first" data-toggle="tab">STUDIO
                                                TAUSYIAH</a></li>
                                        <li><a href="#tab-lv21-second" data-toggle="tab">STUDIO LOUNGE</a></li>
                                        <li><a href="#tab-lv21-third" data-toggle="tab">STUDIO HIBURAN</a></li>
                                        <li><a href="#tab-lv21-last" data-toggle="tab">WEBINAR</a></li>
                                    </ul>
                                </div>
                                <div class="tab-content lv2">
                                    <div id="tab-lv21-first" class="tab-pane fade in active">
                                        <div class="timeline">

                                            {{-- STUDIO TAUSYIAH --}}
                                            @foreach ($studioTausiah as $item)
                                                <article class="post-wrap" data-animation="fadeInUp"
                                                    data-animation-delay="200">
                                                    <div class="text-center">
                                                        {{-- LINK ZOOM --}}
                                                        <a data-animation="flipInY" data-animation-delay="500" href="#"
                                                            class="btn btn-theme">JOIN</a>
                                                    </div>
                                                    <div class="media">
                                                        <!-- -->
                                                        <div class="post-media pull-left">
                                                            {{-- FOTO PEMBICARA / ACARA --}}
                                                            <img src="{{ $item->foto }}" alt="" class="media-object" />
                                                            {{-- <img src="assets/img/preview/avatar-v2-4.jpg" alt=""
                                                                class="media-object" /> --}}
                                                        </div>
                                                        <!-- -->
                                                        <div class="media-body">
                                                            <div class="post-header" style="">
                                                                <div class="post-meta">
                                                                    {{-- WAKTU ACARA --}}
                                                                    <span class="post-date"><i
                                                                            class="fa fa-clock-o"></i>
                                                                        {{ $item->jam }}</span>
                                                                    {{-- <a href="#" class="pull-right">
                                                                <span class="fa-stack fa-lg">
                                                                    <i class="fa fa-stack-2x fa-circle-thin"></i>
                                                                    <i class="fa fa-stack-1x fa-share-alt"></i>
                                                                </span>
                                                            </a> --}}
                                                                </div>
                                                                {{-- NAMA PEMBICARA --}}
                                                                <h2 class="post-title"><a
                                                                        href="#">{{ $item->nama }}</a>
                                                                </h2>
                                                            </div>
                                                            <div class="post-body">
                                                                <div class="post-excerpt">
                                                                    {{-- DESKRIPSI ACARA --}}
                                                                    <p>{{ $item->deskripsi }}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- -->
                                                    </div>
                                                </article>
                                            @endforeach

                                        </div>
                                    </div>

                                    <div id="tab-lv21-second" class="tab-pane fade">
                                        <div class="timeline">

                                            {{-- STUDIO LOUNGE --}}
                                            @foreach ($studioLounge as $item)
                                                <article class="post-wrap" data-animation="fadeInUp"
                                                    data-animation-delay="200">
                                                    <div class="text-center">
                                                        {{-- LINK ZOOM --}}
                                                        <a data-animation="flipInY" data-animation-delay="500" href="#"
                                                            class="btn btn-theme">JOIN</a>
                                                    </div>
                                                    <div class="media">
                                                        <!-- -->
                                                        <div class="post-media pull-left">
                                                            {{-- FOTO PEMBICARA / ACARA --}}
                                                            <img src="{{ $item->foto }}" alt="" class="media-object"  />
                                                            {{-- <img src="assets/img/preview/avatar-v2-4.jpg" alt=""
                                                                class="media-object" /> --}}
                                                        </div>
                                                        <!-- -->
                                                        <div class="media-body">
                                                            <div class="post-header" style="">
                                                                <div class="post-meta">
                                                                    {{-- WAKTU ACARA --}}
                                                                    <span class="post-date"><i
                                                                            class="fa fa-clock-o"></i>
                                                                        {{ $item->jam }}</span>
                                                                    {{-- <a href="#" class="pull-right">
                                                                <span class="fa-stack fa-lg">
                                                                    <i class="fa fa-stack-2x fa-circle-thin"></i>
                                                                    <i class="fa fa-stack-1x fa-share-alt"></i>
                                                                </span>
                                                            </a> --}}
                                                                </div>
                                                                {{-- NAMA PEMBICARA --}}
                                                                <h2 class="post-title"><a
                                                                        href="#">{{ $item->nama }}</a>
                                                                </h2>
                                                            </div>
                                                            <div class="post-body">
                                                                <div class="post-excerpt">
                                                                    {{-- DESKRIPSI ACARA --}}
                                                                    <p>{{ $item->deskripsi }}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- -->
                                                    </div>
                                                </article>
                                            @endforeach

                                        </div>
                                    </div>
                                    <div id="tab-lv21-third" class="tab-pane fade">
                                        <div class="timeline">

                                            {{-- STUDIO HIBURAN --}}
                                            @foreach ($studioHiburan as $item)
                                                <article class="post-wrap" data-animation="fadeInUp"
                                                    data-animation-delay="200">
                                                    <div class="text-center">
                                                        {{-- LINK ZOOM --}}
                                                        <a data-animation="flipInY" data-animation-delay="500" href="#"
                                                            class="btn btn-theme">JOIN</a>
                                                    </div>
                                                    <div class="media">
                                                        <!-- -->
                                                        <div class="post-media pull-left">
                                                            {{-- FOTO PEMBICARA / ACARA --}}
                                                            <img src="{{ $item->foto }}" alt="" class="media-object"
                                                                 />
                                                            {{-- <img src="assets/img/preview/avatar-v2-4.jpg" alt=""
                                                                class="media-object" /> --}}
                                                        </div>
                                                        <!-- -->
                                                        <div class="media-body">
                                                            <div class="post-header" style="">
                                                                <div class="post-meta">
                                                                    {{-- WAKTU ACARA --}}
                                                                    <span class="post-date"><i
                                                                            class="fa fa-clock-o"></i>
                                                                        {{ $item->jam }}</span>
                                                                    {{-- <a href="#" class="pull-right">
                                                                <span class="fa-stack fa-lg">
                                                                    <i class="fa fa-stack-2x fa-circle-thin"></i>
                                                                    <i class="fa fa-stack-1x fa-share-alt"></i>
                                                                </span>
                                                            </a> --}}
                                                                </div>
                                                                {{-- NAMA PEMBICARA --}}
                                                                <h2 class="post-title"><a
                                                                        href="#">{{ $item->nama }}</a>
                                                                </h2>
                                                            </div>
                                                            <div class="post-body">
                                                                <div class="post-excerpt">
                                                                    {{-- DESKRIPSI ACARA --}}
                                                                    <p>{{ $item->deskripsi }}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- -->
                                                    </div>
                                                </article>
                                            @endforeach

                                        </div>
                                    </div>
                                    <div id="tab-lv21-last" class="tab-pane fade">
                                        <div class="timeline">

                                            {{-- WEBINAR --}}
                                            @foreach ($studioTausiah as $item)
                                                <article class="post-wrap" data-animation="fadeInUp"
                                                    data-animation-delay="200">
                                                    <div class="text-center">
                                                        {{-- LINK ZOOM --}}
                                                        <a data-animation="flipInY" data-animation-delay="500" href="#"
                                                            class="btn btn-theme">JOIN</a>
                                                    </div>
                                                    <div class="media">
                                                        <!-- -->
                                                        <div class="post-media pull-left">
                                                            {{-- FOTO PEMBICARA / ACARA --}}
                                                            <img src="{{ $item->foto }}" alt="" class="media-object"
                                                                 />
                                                            {{-- <img src="assets/img/preview/avatar-v2-4.jpg" alt=""
                                                           class="media-object" /> --}}
                                                        </div>
                                                        <!-- -->
                                                        <div class="media-body">
                                                            <div class="post-header" style="">
                                                                <div class="post-meta">
                                                                    {{-- WAKTU ACARA --}}
                                                                    <span class="post-date"><i
                                                                            class="fa fa-clock-o"></i>
                                                                        {{ $item->jam }}</span>
                                                                    {{-- <a href="#" class="pull-right">
                                                           <span class="fa-stack fa-lg">
                                                               <i class="fa fa-stack-2x fa-circle-thin"></i>
                                                               <i class="fa fa-stack-1x fa-share-alt"></i>
                                                           </span>
                                                       </a> --}}
                                                                </div>
                                                                {{-- NAMA PEMBICARA --}}
                                                                <h2 class="post-title"><a
                                                                        href="#">{{ $item->nama }}</a>
                                                                </h2>
                                                            </div>
                                                            <div class="post-body">
                                                                <div class="post-excerpt">
                                                                    {{-- DESKRIPSI ACARA --}}
                                                                    <p>{{ $item->deskripsi }}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- -->
                                                    </div>
                                                </article>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Schedule -->

                </div>
            </section>
            <!-- /PAGE SCHEDULE -->


            <!-- PAGE SPEAKERS -->
            <section class="page-section light" id="speakers">
                <div class="container">
                    <h1 class="section-title">
                        <span data-animation="flipInY" data-animation-delay="300" class="icon-inner"><span
                                class="fa-stack"><i class="fa rhex fa-stack-2x"></i><i
                                    class="fa fa-user fa-stack-1x"></i></span></span>
                        <span data-animation="fadeInUp" data-animation-delay="500" class="title-inner">Event
                            Speakers</span>
                    </h1>

                    <!-- Speakers row -->
                    <div class="row thumbnails clear">

                        {{-- FOREACH PEMBICARA --}}
                        @foreach ($pembicara as $item)
                            <div class="col-sm-6 col-md-3" data-animation="fadeInUp" data-animation-delay="100">
                                <div class="thumbnail no-border no-padding text-center">
                                    <div class="hex">
                                        <div class="hex-deg">
                                            <div class="hex-deg">
                                                <div class="hex-deg">
                                                    <div class="hex-inner">
                                                        <div class="media">
                                                            {{-- FOTO PEMBICARA --}}
                                                            <img src="{{ $item->foto }}" alt="">
                                                            {{-- <img src="assets/img/preview/speaker-1.jpg" alt=""> --}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="caption before-media">
                                        {{-- NAMA PEMBICARA --}}
                                        <h3 class="caption-title">{{ $item->nama }}</h3>
                                        <p class="caption-category">Co Founder</p>
                                    </div>
                                    <div class="caption">
                                        {{-- DESKRIPSI PEMBICARA --}}
                                        <p>{{ $item->deskripsi }}
                                        </p>
                                        {{-- <ul class="social-line list-inline text-center">
                                <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" class="google"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                            </ul> --}}
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                    <!-- /Speakers row -->

                    <div class="text-center margin-top">
                        <a data-animation="fadeInUp" data-animation-delay="100" href="#" class="btn btn-theme"><i
                                class="fa fa-user"></i> See all speakers</a>
                    </div>
                </div>
            </section>
            <!-- /PAGE SPEAKERS -->

            <div class='onesignal-customlink-container'></div>

            {{-- FAQS --}}
            <section class="page-section light">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 pull-left">
                            <h1 class="section-title">
                                <span data-animation="flipInY" data-animation-delay="300" class="icon-inner"><span
                                        class="fa-stack"><i class="fa rhex fa-stack-2x"></i><i
                                            class="fa fa-question fa-stack-1x"></i></span></span>
                                <span data-animation="fadeInRight" data-animation-delay="500" class="title-inner">Event
                                    FAQS</span>
                            </h1>
                        </div>
                        {{-- <div class="col-md-4 text-right-md pull-right">
                        <a href="#" class="btn btn-theme btn-theme-lg btn-theme-transparent-grey pull-right"
                           data-animation="flipInY" data-animation-delay="700"><i class="fa fa-pencil"></i> Open a ticket</a>
                    </div> --}}
                    </div>
                    <div class="row faq margin-top" data-animation="fadeInUp" data-animation-delay="100">
                        <div class="col-sm-6 col-md-6 pull-left">
                            <ul id="tabs-faq" class="nav">
                                <li class="active"><a href="#tab-faq1" data-toggle="tab"><i
                                            class="fa fa-angle-right"></i> <span class="faq-inner">How to Change Event
                                            Date</span></a></li>
                                <li><a href="#tab-faq2" data-toggle="tab"><i class="fa fa-plus"></i> <span
                                            class="faq-inner">How to make New Event ?</span></a></li>
                                <li><a href="#tab-faq3" data-toggle="tab"><i class="fa fa-plus"></i> <span
                                            class="faq-inner">How to Set Price ?</span></a></li>
                                <li><a href="#tab-faq4" data-toggle="tab"><i class="fa fa-plus"></i> <span
                                            class="faq-inner">How to Delete Old Events ?</span></a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-md-6 pull-right">
                            <div class="tab-content">
                                <div id="tab-faq1" class="tab-pane fade in active">
                                    <div>
                                        <p>Vestibulum sit amet tincidunt urna, eget ullamcorper purus. Aenean feugiat
                                            quis
                                            tortor vitae fringilla. Pellentesque augue nisl, condimentum at sem et,
                                            fermentum varius ligula. Nulla dignissim nulla eget congue cursus. </p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                                <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                                <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                            </div>
                                            <div class="col-md-6">
                                                <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                                <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                                <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tab-faq2" class="tab-pane fade">
                                    <div>
                                        <p>Vestibulum sit amet tincidunt urna, eget ullamcorper purus. Aenean feugiat
                                            quis
                                            tortor vitae fringilla. Pellentesque augue nisl, condimentum at sem et,
                                            fermentum varius ligula. Nulla dignissim nulla eget congue cursus. </p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                                <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                                <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                            </div>
                                            <div class="col-md-6">
                                                <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                                <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tab-faq3" class="tab-pane fade">
                                    <div>
                                        <p>Vestibulum sit amet tincidunt urna, eget ullamcorper purus. Aenean feugiat
                                            quis
                                            tortor vitae fringilla. Pellentesque augue nisl, condimentum at sem et,
                                            fermentum varius ligula. Nulla dignissim nulla eget congue cursus. </p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                                <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                                <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                            </div>
                                            <div class="col-md-6">
                                                <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                                <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                                <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tab-faq4" class="tab-pane fade">
                                    <div>
                                        <p>Vestibulum sit amet tincidunt urna, eget ullamcorper purus. Aenean feugiat
                                            quis
                                            tortor vitae fringilla. Pellentesque augue nisl, condimentum at sem et,
                                            fermentum varius ligula. Nulla dignissim nulla eget congue cursus. </p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                                <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                                <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                            </div>
                                            <div class="col-md-6">
                                                <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                                <p><i class="fa fa-check-circle-o"></i> First Awesome Feature</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- /Content area -->


        <!-- FOOTER -->
        <footer class="footer">
            <div class="footer-meta">
                <div class="container text-center">
                    <div class="clearfix">
                    </div>
                    <span class="copyright" data-animation="fadeInUp" data-animation-delay="100">&copy; {{date('Y')}} Mari Ngabuburit
                        &#8212; Powered by Yokesen Teknologi Indonesia</span>
                </div>
            </div>
        </footer>
        <!-- /FOOTER -->

        <div class="to-top"><i class="fa fa-angle-up"></i></div>

    </div>
    <!-- /Wrap all content -->


    <!-- JS Global -->
    <!--[if lt IE 9]><script src="assets/plugins/jquery/jquery-1.11.1.min.js"></script><![endif]-->
    <!--[if gte IE 9]><!-->
    <script src="{{ asset('assets/plugins/jquery/jquery-2.1.1.min.js') }}"></script>
    <!--<![endif]-->
    <script src="{{ asset('assets/plugins/modernizr.custom.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/superfish/js/superfish.js') }}"></script>
    <script src="{{ asset('assets/plugins/prettyphoto/js/jquery.prettyPhoto.js') }}"></script>
    <script src="{{ asset('assets/plugins/placeholdem.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery.smoothscroll.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery.easing.min.js') }}"></script>

    <!-- JS Page Level -->
    <script src="{{ asset('assets/plugins/owlcarousel2/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/waypoints/waypoints.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/countdown/jquery.plugin.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/countdown/jquery.countdown.min.js') }}"></script>
    {{-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script> --}}

    <script src="{{ asset('assets/js/theme-ajax-mail.js') }}"></script>
    <script src="{{ asset('assets/js/theme.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>

    <script type="text/javascript">
        jQuery(document).ready(function() {
            theme.init();
            theme.initMainSlider();
            theme.initCountDown();
            theme.initPartnerSlider();
            theme.initTestimonials();
            // theme.initGoogleMap();
        });
        jQuery(window).load(function() {
            theme.initAnimation();
        });

        jQuery(window).load(function() {
            jQuery('body').scrollspy({
                offset: 100,
                target: '.navigation'
            });
        });
        jQuery(window).load(function() {
            jQuery('body').scrollspy('refresh');
        });
        jQuery(window).resize(function() {
            jQuery('body').scrollspy('refresh');
        });

        jQuery(document).ready(function() {
            theme.onResize();
        });
        jQuery(window).load(function() {
            theme.onResize();
        });
        jQuery(window).resize(function() {
            theme.onResize();
        });

        jQuery(window).load(function() {
            if (location.hash != '') {
                var hash = '#' + window.location.hash.substr(1);
                if (hash.length) {
                    jQuery('html,body').delay(0).animate({
                        scrollTop: jQuery(hash).offset().top - 44 + 'px'
                    }, {
                        duration: 1200,
                        easing: "easeInOutExpo"
                    });
                }
            }
        });

    </script>

</body>

</html>
