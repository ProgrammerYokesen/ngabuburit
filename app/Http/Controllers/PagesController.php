<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class PagesController extends Controller
{
    public  function homePage() {
        $currentDate = Carbon::now()->format('Y-m-d');
        
        // studio tausiah
        $studioTausiah = DB::table('studio_tausiah')->where('tanggal', $currentDate)->get();
        
        // studio hiburan
        $studioHiburan = DB::table('studio_hiburan')->where('tanggal', $currentDate)->get();
        
        // studio lounge
        $studioLounge = DB::table('studio_lounge')->where('tanggal', $currentDate)->get();
        
        // Link
        $link = DB::table('link')->first();
        
        // pembicara
        $pembicara = DB::table('pembicara')->get();
        // dd($pembicara);
        
        // webinar
        $webinar = DB::table('webinar')->where('tanggal', $currentDate)->get();
        
        // faq
        $faq = DB::table('faq')->get();
        
        return view('pages.app', compact('studioTausiah', 'studioHiburan', 'studioLounge', 'link', 'pembicara', 'webinar', 'faq'));
    }
}
